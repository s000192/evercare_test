## Evercare Engineering Test

### System requirements
Please ensure you have installed Node.js version 8 or above.

### Introduction
The task is to create a JavaScript selection engine i.e. a JavaScript function that will return DOM elements given a CSS selector.

The test contains 3 files
* **index.html** contains the HTML your function will be tested on
* **answer.js** contains a template function that you should change
* **spec.js** contains a template test spec that you should write your tests in

### Rules
1. You should only modify answer.js and spec.js
2. You may not use any JavaScript libraries in answer.js
3. document.querySelector/document.querySelectorAll may not be used
4. The testing framework used is mocha. Feel free to choose any assertion library that you feel comfortable using.

### Setup

```bash
npm install
```

### Writing your solution

The template **answer.js** looks like this:

```js
/**
 * @param {string} selector - a CSS selector string representing the target DOM element, e.g. 'div.class'
 * @param {Object} dom - an object representation of the DOM (returned by JS DOM) being iterated over
*/

function $ (selector, dom) { 
    const elements = [];
    
    ////////////////////
    // Your code here //
    ////////////////////
    
    return elements;
}

```

**answer.js** will be tested against **test.html**

```html
<html>
  <head>
    <script src="Answer.js"></script>
    <script src="Test.js"></script>
  </head>
  <body onload="test$()">
    <div></div>
    <div id="some_id" class="some_class some_other_class">
      <div id="some_id">
        <div id="some_id" class="some_class"></div>
      </div>
    </div>
    <img id="some_other_id" class="some_class some_other_class"></img>
    <div class="some_other_class">
      <input type="text">
    </div>
  </body>
</html> 

```

### Writing your tests

Scaffolding for your test file, **spec.js**, is ready for you to include your tests in. 

Your function should return an array of DOM elements that match the CSS selector and cover the following calls:
```js
$("div", dom)
$("img.some_class", dom)
$("#some_parent_id", dom)
$("#some_child_id", dom)
$(".some_class", dom)
$("input#some_id", dom)
$("div#some_id.some_class", dom)
$("div.some_class#some_id", dom)
```

You may use chai.js or any other assertion library of your choice for your tests. 

### BONUS:

Using a bundling tool of your choice, bundle answer.js so that it can run in the browser