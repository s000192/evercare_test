function $(selector, node) {

  let elements = [];

  let idIndex = selector.indexOf('#');
  let classIndex = selector.indexOf('.');

  // only Tag
  if (classIndex < 0 && idIndex < 0) {
    let result = node.window.document.getElementsByTagName(selector);
    if (result) {
      return Array.from(result);
    }
  }

  // only Id
  else if (classIndex < 0 && idIndex === 0) {
    elements.push(node.window.document.getElementById(selector.slice(1)));
  }

  // only Class
  else if (classIndex === 0 && idIndex < 0) {
    let result = node.window.document.getElementsByClassName(selector.slice(1));
    if (result) {
      return Array.from(result);
    }
  }

  // Id involving in the middle of the selector
  else if ( idIndex > 0 ) {
    // first part is either class or tag
    if ( classIndex <= 0 ) {
      let separated = selector.split('#');
      let result1 = $(separated[0], node);
      result1.forEach((e) => {
        if ( e.id === separated[1] ) {
          elements.push(e);
        }
      })
    }

    // both class and id included with tag in the query
    else if ( classIndex > 0 ) {
        let id = selector.substring( idIndex, idIndex > classIndex? selector.length : classIndex);
        let _class = selector.substring( classIndex, idIndex > classIndex? idIndex : selector.length ) ;
        let tag = selector.substring( idIndex > classIndex? classIndex : idIndex )
        let temp = $(id, node)[0];
        if (temp && temp.nodeName == tag && temp.classList.contains(_class)) {
          elements.push(temp);
        }
    }
  }

  // . symbol in the middle
  else if ( classIndex > 0 && idIndex <= 0 ) {
    let separated = selector.split('.');
    let result1 = $(separated[0], node);
    let result2 = result1.filter(e => e.classList.contains('some_class'));
    result2.map((e) => {elements.push(e)});
  }

  return elements;
}

module.exports = $;