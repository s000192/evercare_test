const $ = require('./answer');
const chai = require('chai'); // can replace chai with any assertion library of your choice
const { JSDOM } = require('jsdom');
const fs = require('fs');

const html = fs.readFileSync("index.html").toString();
const dom = new JSDOM(html);

describe('DOM selector', () => {

  it('should run with no errors', () => {
    $('div', dom);
  });

  it('should return all div elements', () => {
    let result = $('div', dom);
    expect(result.length).toEqual(dom.window.document.querySelectorAll('div').length);
    $('div', dom).map((e) => {
      expect(e.nodeName).toEqual("DIV");
    })
  });

  it('should run with no errors', () => {
    $("img.some_class", dom)
  });

  it("should return img elements with class of 'some_class'", () => {
    let result = $('img.some_class', dom);
    expect(result.length).toEqual(dom.window.document.querySelectorAll('img.some_class').length);
    result.map((e) => {
      expect(e.nodeName).toEqual("IMG");
    })
  });

  it('should run with no errors', () => {
    $("#some_parent_id", dom)
  });

  it("should return elements with id of 'some_parent_id'", () => {
    let result = $('#some_parent_id', dom);
    expect(result.length).toEqual(dom.window.document.querySelectorAll('#some_parent_id').length);
    result.map((e) => {
      expect(e.id).toEqual('some_parent_id');
    })
  });

  it('should run with no errors', () => {
    $("#some_child_id", dom)
  });

  it("should return elements with id of 'some_children_id'", () => {
    let result = $('#some_child_id', dom);
    expect(result.length).toEqual(dom.window.document.querySelectorAll('#some_parent_id').length);
    result.map((e) => {
      expect(e.id).toEqual('some_child_id');
    })
  });

  it('should run with no errors', () => {
    $("input#some_id", dom)
  });

  it("should return an empty list when querying 'input#some_id'", () => {
    let result = $('input#some_id', dom);
    expect(result.length).toEqual(dom.window.document.querySelectorAll('input#some_id').length);
    result.map((e) => {
      expect(e.nodeName).toEqual("INPUT");
    })
  });

  it('should run with no errors', () => {
    $(".some_class", dom)
  });

  it("should return 3 elements with class '.some_class'", () => {
    let result = $('.some_class', dom);
    expect(result.length).toEqual(dom.window.document.querySelectorAll('.some_class').length);
    result.map((e) => {
      expect(e.classList.contains("some_class")).toEqual(true);
    })
  });

  it('should run with no errors', () => {
    $("div#some_id.some_class", dom)
  });

  it("should return a div element when querying 'div#some_id.some_class'", () => {
    let result = $('div#some_id.some_class', dom);
    expect(result.length).toEqual(dom.window.document.querySelectorAll('div#some_id.some_class').length);
    result.map((e) => {
      expect(e.nodeName).toEqual("DIV");
      expect(e.classList.contains("some_class")).toEqual(true);
      expect(e.id).toEqual("some_id");
    })
  });

  it('should run with no errors', () => {
    $("div.some_class#some_id", dom)
  });

  it("should return a div element when querying 'ddiv.some_class#some_id'", () => {
    let result = $('div.some_class#some_id', dom);
    expect(result.length).toEqual(dom.window.document.querySelectorAll('div.some_class#some_id').length);
    result.map((e) => {
      expect(e.nodeName).toEqual("DIV");
      expect(e.classList.contains("some_class")).toEqual(true);
      expect(e.id).toEqual("some_id");
    })
  });
});